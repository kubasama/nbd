import scala.annotation.tailrec

object NBD1 extends App {
  val days: List[String] = List("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
  println("NBD1 Task 1a:")
  println(task1a)
  println("\nNBD1 Task 1b:")
  println(task1b)
  println("\nNBD1 Task 1c:")
  println(task1c)
  println("\nNBD1 Task 2a:")
  println(task2a(days))
  println("\nNBD1 Task 2b:")
  println(task2b(days))
  println("\nNBD1 Task 3:")
  println(task3(days))


  def task1a(): String = {
    var out: String = ""
    for (day <- days)
      out += day + ", "
    return out
  }
  def task1b(): String = {
    var out: String = ""
    for (day <- days)
      if(day.toLowerCase.startsWith("s")){
        out += day + ", "
      }

    return out
  }
  def task1c(): String = {
    var out: String = ""
    var i: Int =0;
    while (i < days.length) {

      out += days(i) + ", "
      i = i+1;
    }


    return out
  }

  def task2a( iter: List[String]): String = {
    //var out: String = ""
    //var i: Int =0;
    if(iter.tail.isEmpty)
  return iter.head
    else return iter.head+", "+task2a(iter.tail)

    //return out
  }
  def task2b( iter: List[String]): String = {
    //var out: String = ""
    //var i: Int =0;
    if(iter.tail.isEmpty)
      return iter.head
    else return task2b(iter.tail)+", "+iter.head

    //return out
  }
  def task3( iter: List[String]): String = {
    //var out: String = ""
    //var i: Int =0;
    @tailrec
    def task3inner(lis: List[String],res:String): String = {
      if (lis.tail.isEmpty)
        return res+lis.head
      else   task3inner(lis.tail,res+lis.head + ", ")
    }
    task3inner(iter,"")
  }
}